package com.manuelpeinado.droidcast.test;

import android.support.v4.view.ViewPager;
import android.test.ActivityInstrumentationTestCase2;

import com.manuelpeinado.droidcast.activities.ForecastActivity;
import com.manuelpeinado.droidcast.persistence.CitiesDao;

public class ForecastActivityTest extends ActivityInstrumentationTestCase2<ForecastActivity> {

	public ForecastActivityTest() {
		super(ForecastActivity.class);
	}

	public void testViewPagerHasCorrectNumberOfPages() {
		ForecastActivity a = getActivity();
		ViewPager viewPager = (ViewPager) a.findViewById(com.manuelpeinado.droidcast.R.id.viewPager);
		int numCities = new CitiesDao().getCount();
		int numPages = viewPager.getAdapter().getCount();
		assertEquals(numCities, numPages);
	}

}
