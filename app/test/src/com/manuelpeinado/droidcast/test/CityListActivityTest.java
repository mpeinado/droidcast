package com.manuelpeinado.droidcast.test;

import android.support.v4.app.Fragment;
import android.test.ActivityInstrumentationTestCase2;

import com.manuelpeinado.droidcast.activities.CityListActivity;
import com.manuelpeinado.droidcast.persistence.Prefs;

public class CityListActivityTest extends ActivityInstrumentationTestCase2<CityListActivity> {

	public CityListActivityTest() {
		super(CityListActivity.class);
	}
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		Prefs.clearLastActivity();
	}
	
	public void testCityListFragmentPresent() {
		CityListActivity a = getActivity();
		Fragment f = a.getSupportFragmentManager().findFragmentById(com.manuelpeinado.droidcast.R.id.cityListFragment);
		assertNotNull(f);
	}

}
