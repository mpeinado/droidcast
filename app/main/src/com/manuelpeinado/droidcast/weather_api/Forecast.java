package com.manuelpeinado.droidcast.weather_api;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;

/**
 * Response of the World Weather Online web service, to be mapped with GSON
 * Generated automatically using http://jsongen.byingtondesign.com/
 */
public class Forecast implements Parcelable {
	private Data data;

	public Data getData() {
		return this.data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	public WeatherCondition getCurrentCondition() {
		return data.getCurrentCondition().get(0);
	}
	
	public Weather getWeather(int position) {
		return data.getWeather().get(position);
	}


	public int describeContents() {
		return 0;
	}

	public void writeToParcel(Parcel dest, int flags) {
		Gson gson = new Gson();
		dest.writeString(gson.toJson(this));
	}

	public static final Parcelable.Creator<Forecast> CREATOR = new Parcelable.Creator<Forecast>() {
		public Forecast createFromParcel(Parcel in) {
			Gson gson = new Gson();
			return gson.fromJson(in.readString(), Forecast.class);
		}

		public Forecast[] newArray(int size) {
			return new Forecast[size];
		}
	};
}
