package com.manuelpeinado.droidcast.weather_api;

import java.util.List;

public class Weather {
	private String date;
	private String precipMM;
	private String tempMaxC;
	private String tempMaxF;
	private String tempMinC;
	private String tempMinF;
	private String weatherCode;
	private List<WeatherDesc> weatherDesc;
	private List<WeatherIconUrl> weatherIconUrl;
	private String winddir16Point;
	private String winddirDegree;
	private String winddirection;
	private String windspeedKmph;
	private String windspeedMiles;

	public String getDate() {
		return this.date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getPrecipMM() {
		return this.precipMM;
	}

	public void setPrecipMM(String precipMM) {
		this.precipMM = precipMM;
	}

	public String getTempMaxC() {
		return this.tempMaxC;
	}

	public void setTempMaxC(String tempMaxC) {
		this.tempMaxC = tempMaxC;
	}

	public String getTempMaxF() {
		return this.tempMaxF;
	}

	public void setTempMaxF(String tempMaxF) {
		this.tempMaxF = tempMaxF;
	}

	public String getTempMinC() {
		return this.tempMinC;
	}

	public void setTempMinC(String tempMinC) {
		this.tempMinC = tempMinC;
	}

	public String getTempMinF() {
		return this.tempMinF;
	}

	public void setTempMinF(String tempMinF) {
		this.tempMinF = tempMinF;
	}

	public String getWeatherCode() {
		return this.weatherCode;
	}

	public void setWeatherCode(String weatherCode) {
		this.weatherCode = weatherCode;
	}

	public List<WeatherDesc> getWeatherDesc() {
		return this.weatherDesc;
	}

	public void setWeatherDesc(List<WeatherDesc> weatherDesc) {
		this.weatherDesc = weatherDesc;
	}

	public List<WeatherIconUrl> getWeatherIconUrl() {
		return this.weatherIconUrl;
	}

	public void setWeatherIconUrl(List<WeatherIconUrl> weatherIconUrl) {
		this.weatherIconUrl = weatherIconUrl;
	}

	public String getWinddir16Point() {
		return this.winddir16Point;
	}

	public void setWinddir16Point(String winddir16Point) {
		this.winddir16Point = winddir16Point;
	}

	public String getWinddirDegree() {
		return this.winddirDegree;
	}

	public void setWinddirDegree(String winddirDegree) {
		this.winddirDegree = winddirDegree;
	}

	public String getWinddirection() {
		return this.winddirection;
	}

	public void setWinddirection(String winddirection) {
		this.winddirection = winddirection;
	}

	public String getWindspeedKmph() {
		return this.windspeedKmph;
	}

	public void setWindspeedKmph(String windspeedKmph) {
		this.windspeedKmph = windspeedKmph;
	}

	public String getWindspeedMiles() {
		return this.windspeedMiles;
	}

	public void setWindspeedMiles(String windspeedMiles) {
		this.windspeedMiles = windspeedMiles;
	}

	public String getDescription() {
		return weatherDesc.get(0).getValue();
	}
}
