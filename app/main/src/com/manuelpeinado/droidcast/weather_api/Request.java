package com.manuelpeinado.droidcast.weather_api;


public class Request{
   	private String query;
   	private String type;

 	public String getQuery(){
		return this.query;
	}
	public void setQuery(String query){
		this.query = query;
	}
 	public String getType(){
		return this.type;
	}
	public void setType(String type){
		this.type = type;
	}
}
