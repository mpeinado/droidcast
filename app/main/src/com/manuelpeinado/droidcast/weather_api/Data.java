package com.manuelpeinado.droidcast.weather_api;

import java.util.List;

public class Data {
   	private List<WeatherCondition> current_condition;
   	private List<Request> request;
   	private List<Weather> weather;

 	public List<WeatherCondition> getCurrentCondition(){
		return this.current_condition;
	}
	public void setCurrent_condition(List<WeatherCondition> current_condition){
		this.current_condition = current_condition;
	}
 	public List<Request> getRequest(){
		return this.request;
	}
	public void setRequest(List<Request> request){
		this.request = request;
	}
 	public List<Weather> getWeather(){
		return this.weather;
	}
	public void setWeather(List<Weather> weather){
		this.weather = weather;
	}
}
