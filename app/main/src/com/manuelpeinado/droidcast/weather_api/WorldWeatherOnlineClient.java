package com.manuelpeinado.droidcast.weather_api;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.manuelpeinado.caching.CachingHttpClient;
import com.manuelpeinado.droidcast.DroidcastApplication;
import com.manuelpeinado.droidcast.persistence.CacheManager;
import com.manuelpeinado.droidcast.persistence.City;
import com.manuelpeinado.utils.Callback;

/**
 * Finds the weather forecast for a given city implemented with the World
 * Weather Online API See {@link http://www.worldweatheronline.com/free-weather.aspx}
 * Stores the responses in a disk LRU cache to save bandwidth
 */
public class WorldWeatherOnlineClient {
	public static final String TAG = WorldWeatherOnlineClient.class.getSimpleName();
	private static final String BASE_URL = "http://free.worldweatheronline.com/feed/weather.ashx";
	/** Obtained by creating an account at http://www.worldweatheronline.com */
	private static final String API_KEY = "4d501bebe0162434130303";
	private boolean ignoreCache;

	public void getForecast(City city, final Callback<Forecast> responseHandler) {
		RequestParams params = buildRequestParams(city);
		String url = AsyncHttpClient.getUrlWithQueryString(BASE_URL, params);
		final String cacheKey = getCacheKey(city);
		CachingHttpClient client = CacheManager.getHttpClient();
		if (ignoreCache) {
			client.removeFromCache(cacheKey);
		}
		client.get(url, cacheKey, new Callback<String>() {
			@Override
			public void call(String responseText) {
				Gson gson = new Gson();
				Forecast response = gson.fromJson(responseText, Forecast.class);
				responseHandler.call(response);
			}
		});
	}
	
	public void setIgnoreCache(boolean value) {
		this.ignoreCache = value;
	}
	
	private String getCacheKey(City city) {
		return city.getName().toLowerCase().replace(" ", "");
	}

	private RequestParams buildRequestParams(City city) {
		RequestParams params = new RequestParams();
		params.put("q", city.getName());
		params.put("format", "json");
		params.put("num_of_days", Integer.toString(DroidcastApplication.NUM_DAYS));
		params.put("key", API_KEY);
		return params;
	}
}
