package com.manuelpeinado.droidcast.weather_api;

import java.util.List;

public class WeatherCondition {
	private String cloudcover;
	private String humidity;
	private String observation_time;
	private String precipMM;
	private String pressure;
	private String temp_C;
	private String temp_F;
	private String visibility;
	private String weatherCode;
	private List<WeatherDesc> weatherDesc;
	private List<WeatherIconUrl> weatherIconUrl;
	private String winddir16Point;
	private String winddirDegree;
	private String windspeedKmph;
	private String windspeedMiles;

	public String getCloudcover() {
		return this.cloudcover;
	}

	public void setCloudcover(String cloudcover) {
		this.cloudcover = cloudcover;
	}

	public String getHumidity() {
		return this.humidity;
	}

	public void setHumidity(String humidity) {
		this.humidity = humidity;
	}

	public String getObservation_time() {
		return this.observation_time;
	}

	public void setObservation_time(String observation_time) {
		this.observation_time = observation_time;
	}

	public String getPrecipMM() {
		return this.precipMM;
	}

	public void setPrecipMM(String precipMM) {
		this.precipMM = precipMM;
	}

	public String getPressure() {
		return this.pressure;
	}

	public void setPressure(String pressure) {
		this.pressure = pressure;
	}

	public String getTemp_C() {
		return this.temp_C;
	}

	public void setTemp_C(String temp_C) {
		this.temp_C = temp_C;
	}

	public String getTemp_F() {
		return this.temp_F;
	}

	public void setTemp_F(String temp_F) {
		this.temp_F = temp_F;
	}

	public String getVisibility() {
		return this.visibility;
	}

	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}

	public String getWeatherCode() {
		return this.weatherCode;
	}

	public void setWeatherCode(String weatherCode) {
		this.weatherCode = weatherCode;
	}

	public String getDescription() {
		return weatherDesc.get(0).getValue();
	}

	public List<WeatherIconUrl> getWeatherIconUrl() {
		return this.weatherIconUrl;
	}

	public void setWeatherIconUrl(List<WeatherIconUrl> weatherIconUrl) {
		this.weatherIconUrl = weatherIconUrl;
	}

	public String getWinddir16Point() {
		return this.winddir16Point;
	}

	public void setWinddir16Point(String winddir16Point) {
		this.winddir16Point = winddir16Point;
	}

	public String getWinddirDegree() {
		return this.winddirDegree;
	}

	public void setWinddirDegree(String winddirDegree) {
		this.winddirDegree = winddirDegree;
	}

	public String getWindspeedKmph() {
		return this.windspeedKmph;
	}

	public void setWindspeedKmph(String windspeedKmph) {
		this.windspeedKmph = windspeedKmph;
	}

	public String getWindspeedMiles() {
		return this.windspeedMiles;
	}

	public void setWindspeedMiles(String windspeedMiles) {
		this.windspeedMiles = windspeedMiles;
	}

	public String getIconUrl() {
		List<WeatherIconUrl> icons = getWeatherIconUrl();
		if (icons.size() == 0) {
			return null;
		}
		return icons.get(0).getValue();
	}
}
