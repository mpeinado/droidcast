package com.manuelpeinado.droidcast.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.manuelpeinado.droidcast.DroidcastApplication;
import com.manuelpeinado.droidcast.R;
import com.manuelpeinado.droidcast.dialogs.AddCityDialog;
import com.manuelpeinado.droidcast.fragments.CityListFragment;
import com.manuelpeinado.droidcast.persistence.CitiesDao;
import com.manuelpeinado.droidcast.persistence.City;
import com.manuelpeinado.droidcast.persistence.Prefs;
import com.manuelpeinado.undobar.IUndoActivity;
import com.manuelpeinado.undobar.UndoBarController;
import com.manuelpeinado.undobar.UndoBarController.UndoListener;
import com.manuelpeinado.utils.Utils;

/**
 * Simple container for {@link CityListFragment}
 */
public class CityListActivity extends SherlockFragmentActivity implements IUndoActivity, UndoListener, CityListFragment.Listener, AddCityDialog.Listener {
	private UndoListener undoListener;
	private UndoBarController undoBarController;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (startForecastActivityIfNecessary()) {
			return;
		}
		setContentView(R.layout.activity_city_list);
		Prefs.setLastActivity(DroidcastApplication.CITY_LIST_ACTIVITY);
		undoBarController = new UndoBarController(findViewById(R.id.undobar), this);
		FragmentManager fragmentManager = getSupportFragmentManager();
		CityListFragment fragment = (CityListFragment) fragmentManager.findFragmentById(R.id.cityListFragment);
		fragment.setListener(this);
	}

	private boolean startForecastActivityIfNecessary() {
		switch (Prefs.getLastActivity()) {
		case DroidcastApplication.FORECAST_ACTIVITY:
			finish();
			startForecastActivity(null);
			return true;
		}
		return false;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getSupportMenuInflater().inflate(R.menu.activity_city_list, menu);
		return true;
	}

	@Override
	public void showUndoBar(UndoListener listener, String message, String undoToken) {
		this.undoListener = listener;
		undoBarController.showUndoBar(true, message, undoToken);
	}

	@Override
	public void onUndo(String token) {
		if (undoListener != null) {
			undoListener.onUndo(token);
		}
	}

	@Override
	public void onCityClicked(City city) {
		startForecastActivity(city);
	}

	private void startForecastActivity(City city) {
		Intent i = new Intent(this, ForecastActivity.class);
		if (city != null) {
			i.putExtra("city", city);
		}
		startActivity(i);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.add_city:
			showAddCityDialog();
		}
		return false;
	}

	private void showAddCityDialog() {
		AddCityDialog dlg = new AddCityDialog();
		dlg.setListener(this);
		dlg.show(getSupportFragmentManager(), "addCity");
	}

	@Override
	public void onCityAccepted(String cityName) {
		if (cityNameAlreadyExists(cityName)) {
			Utils.shortToast(R.string.city_already_exists);
			return;
		}
		if (!isValidCityName(cityName)) {
			Utils.shortToast(R.string.city_name_not_valid);
		}
		addCity(cityName);
	}

	private boolean cityNameAlreadyExists(String cityName) {
		CitiesDao dao = new CitiesDao();
		return dao.queryCityByName(cityName) != null;
	}

	private boolean isValidCityName(String cityName) {
		// TODO check this using Google Places API or Geonames API
		return true;
	}

	private void addCity(String cityName) {
		new CitiesDao().add(cityName);
	}
}
