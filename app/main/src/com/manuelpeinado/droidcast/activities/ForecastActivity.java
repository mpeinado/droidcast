package com.manuelpeinado.droidcast.activities;

import java.util.List;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.manuelpeinado.droidcast.DroidcastApplication;
import com.manuelpeinado.droidcast.R;
import com.manuelpeinado.droidcast.fragments.CityForecastFragment;
import com.manuelpeinado.droidcast.fragments.CityListFragment;
import com.manuelpeinado.droidcast.persistence.CitiesDao;
import com.manuelpeinado.droidcast.persistence.City;
import com.manuelpeinado.droidcast.persistence.Prefs;
import com.viewpagerindicator.PageIndicator;

/**
 * Displays a view pager in which each page shows the weather of a city
 */
public class ForecastActivity extends SherlockFragmentActivity 
							  implements LoaderManager.LoaderCallbacks<Cursor>, OnPageChangeListener {
	private PagerAdapter viewPagerAdapter;
	private List<City> cities;
	private PageIndicator pageIndicator;
	private ViewPager viewPager;
	private City initialCity;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_forecast);
		getSupportLoaderManager().initLoader(0, null, this);
		Prefs.setLastActivity(DroidcastApplication.FORECAST_ACTIVITY);
		initActionBar();
		initViewPager();
	}

	private void initActionBar() {
		ActionBar ab = getSupportActionBar();
		ab.setHomeButtonEnabled(true);
		ab.setDisplayHomeAsUpEnabled(true);
	}

	private void initViewPager() {
		setInitialPage();
		viewPagerAdapter = new CitiesPagerAdapter();
		viewPager = (ViewPager) findViewById(R.id.viewPager);
		viewPager.setAdapter(viewPagerAdapter);
		pageIndicator = (PageIndicator) findViewById(R.id.indicator);
		pageIndicator.setViewPager(viewPager);
		pageIndicator.setOnPageChangeListener(this);
	}

	private void setInitialPage() {
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			initialCity = extras.getParcelable("city");
			return;
		}
		String cityName = Prefs.getLastSelectedCity();
		if (cityName != null) {
			initialCity = new City(cityName);
		}
	}

	private class CitiesPagerAdapter extends FragmentPagerAdapter {
		public CitiesPagerAdapter() {
			super(getSupportFragmentManager());
		}

		@Override
		public int getCount() {
			return cities == null ? 0 : cities.size();
		}

		@Override
		public Fragment getItem(int position) {
			City city = cities.get(position);
			return CityForecastFragment.newInstance(city);
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return cities.get(position).getName();
		}
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		return CityListFragment.createLoader();
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		cities = CitiesDao.loadAll(cursor);
		viewPagerAdapter.notifyDataSetChanged();
		pageIndicator.notifyDataSetChanged();
		int index = findCityIndex(initialCity);
		initialCity = null;
		if (index != -1) {
			pageIndicator.setCurrentItem(index);
		}
	}

	private int findCityIndex(City desired) {
		if (cities != null) {
			int i = 0;
			for (City city : cities) {
				if (city.hasSameNameAs(desired)) {
					return i;
				}
				++i;
			}
		}
		return -1;
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getSupportMenuInflater().inflate(R.menu.activity_forecast, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.refresh:
			CityForecastFragment fragment = getCurrentFragment();
			fragment.refreshForecast();
			return true;
		case android.R.id.home:
			jumpToParentActivity();
			return true;
		}
		return false;
	}

	// http://developer.android.com/training/implementing-navigation/ancestral.html
	private void jumpToParentActivity() {
		Intent parentActivityIntent = new Intent(this, CityListActivity.class);
		parentActivityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
		Prefs.clearLastActivity();
		startActivity(parentActivityIntent);
		finish();
	}

	private CityForecastFragment getCurrentFragment() {
		int index = viewPager.getCurrentItem();
		return (CityForecastFragment) viewPagerAdapter.instantiateItem(viewPager, index);
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {
	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
	}

	@Override
	public void onPageSelected(int page) {
		if (cities == null) {
			return;
		}
		City selectedCity = cities.get(page);
		Prefs.setLastSelectedCity(selectedCity.getName());
	}

}
