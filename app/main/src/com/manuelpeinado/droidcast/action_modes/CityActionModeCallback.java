package com.manuelpeinado.droidcast.action_modes;

import com.actionbarsherlock.view.ActionMode;
import com.actionbarsherlock.view.ActionMode.Callback;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.manuelpeinado.droidcast.R;
import com.manuelpeinado.droidcast.fragments.CityListFragment;

/**
 * Manages the selection of cities inside a {@link CityListFragment}
 */
public class CityActionModeCallback implements Callback {
	
	private final CityListFragment owner;

	public CityActionModeCallback(CityListFragment owner) {
		this.owner = owner;
	}
	
	@Override
	public boolean onCreateActionMode(ActionMode mode, Menu menu) {
		mode.getMenuInflater().inflate(R.menu.actionmode_city, menu);
		owner.startEditMode();
		return true;
	}

	@Override
	public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
		return false;
	}

	@Override
	public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
		switch (item.getItemId()) {
		case R.id.delete:
			deleteSelectedCities();
			return true;
		}
		return false;
	}

	@Override
	public void onDestroyActionMode(ActionMode mode) {
		owner.clearSelection();
		owner.endEditMode();
	}
	
	private void deleteSelectedCities() {
		owner.deleteSelectedCities();
	}
}
