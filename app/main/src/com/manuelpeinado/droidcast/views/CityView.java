package com.manuelpeinado.droidcast.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.manuelpeinado.droidcast.R;
import com.manuelpeinado.droidcast.persistence.City;

/**
 * Displays the name of a city and a checkbox to select it
 */
public class CityView extends LinearLayout implements OnCheckedChangeListener, View.OnClickListener {

	private TextView nameView;
	private CheckBox checkBox;
	private Listener listener;
	private City city;

	public interface Listener {
		void onCityChecked(CityView sender, boolean isChecked);
		void onCityClicked(CityView sender, City city);
	}

	public CityView(Context context) {
		this(context, null);
	}

	public CityView(Context context, AttributeSet attrs) {
		super(context, attrs);
		LayoutInflater.from(context).inflate(R.layout.view_city, this);
		nameView = (TextView) findViewById(R.id.name);
		setOnClickListener(this);
		checkBox = (CheckBox) findViewById(R.id.checkbox);
		checkBox.setOnCheckedChangeListener(this);
	}

	public void setCity(City city) {
		this.city = city;
		nameView.setText(city.getName());
	}

	public City getCity() {
		return city;
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if (listener != null) {
			listener.onCityChecked(this, isChecked);
		}
	}

	public void setListener(Listener listener) {
		this.listener = listener;
	}

	@Override
	public void onClick(View view) {
		if (listener != null) {
			listener.onCityClicked(this, city);
		}
	}

	public void uncheck(boolean notify) {
		Listener copy = listener;
		if (!notify) {
			listener = null;
		}
		checkBox.setChecked(false);
		listener = copy;
	}
}
