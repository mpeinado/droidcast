package com.manuelpeinado.droidcast.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.manuelpeinado.droidcast.R;
import com.manuelpeinado.droidcast.weather_api.Weather;

/**
 * Displays the weather forecast for a given location
 */
public class DailyForecastView extends FrameLayout {

	private TextView dateView;
	private TextView descriptionView;
	private KeyValuePairView maxTempView;
	private KeyValuePairView minTempView;
	private KeyValuePairView precipitationView;
	private KeyValuePairView windSpeedView;

	public DailyForecastView(Context context) {
		this(context, null);
	}

	public DailyForecastView(Context context, AttributeSet attrs) {
		super(context, attrs);
		LayoutInflater.from(context).inflate(R.layout.view_daily_forecast, this);
		dateView = (TextView)findViewById(R.id.date);
		descriptionView = (TextView)findViewById(R.id.description);
		maxTempView = (KeyValuePairView)findViewById(R.id.maxTemp);
		minTempView = (KeyValuePairView)findViewById(R.id.minTemp);
		precipitationView = (KeyValuePairView)findViewById(R.id.precipitation);
		windSpeedView = (KeyValuePairView)findViewById(R.id.windSpeed);
	}

	public void setWeather(Weather weather) {
		dateView.setText(weather.getDate());
		descriptionView.setText(weather.getDescription());
		maxTempView.setValue(weather.getTempMaxC());
		minTempView.setValue(weather.getTempMinC());
		precipitationView.setValue(weather.getPrecipMM());
		windSpeedView.setValue(weather.getWindspeedKmph());
	}
}
