package com.manuelpeinado.droidcast.views;

import uk.co.senab.bitmapcache.CacheableBitmapDrawable;
import uk.co.senab.bitmapcache.CacheableImageView;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.manuelpeinado.droidcast.R;
import com.manuelpeinado.droidcast.persistence.CacheManager;
import com.manuelpeinado.droidcast.weather_api.WeatherCondition;
import com.manuelpeinado.utils.Callback;

/**
 * Displays the current weather conditions for a given location
 */
public class CurrentConditionView extends FrameLayout {

	private CacheableImageView imageView;
	private TextView descriptionView;
	private KeyValuePairView temperatureView;
	private KeyValuePairView precipitationView;
	private KeyValuePairView humidityView;
	private KeyValuePairView windSpeedView;

	public CurrentConditionView(Context context, AttributeSet attrs) {
		super(context, attrs);
		LayoutInflater.from(context).inflate(R.layout.view_current_condition, this);
		descriptionView = (TextView) findViewById(R.id.currentConditionDescription);
		imageView = (CacheableImageView) findViewById(R.id.currentConditionIcon);
		temperatureView = (KeyValuePairView)findViewById(R.id.temperature);
		precipitationView = (KeyValuePairView)findViewById(R.id.precipitation);
		humidityView = (KeyValuePairView)findViewById(R.id.humidity);
		windSpeedView = (KeyValuePairView)findViewById(R.id.windSpeed);
	}

	public void setCurrentCondition(WeatherCondition currentCondition) {
		descriptionView.setText(currentCondition.getDescription());
		temperatureView.setValue(currentCondition.getTemp_C());
		precipitationView.setValue(currentCondition.getPrecipMM());
		humidityView.setValue(currentCondition.getHumidity());
		windSpeedView.setValue(currentCondition.getWindspeedKmph());
		
		final String url = currentCondition.getIconUrl();
		CacheManager.getImageLoader().getDrawable(url, new Callback<CacheableBitmapDrawable>() {
			@Override
			public void call(CacheableBitmapDrawable result) {
				if (result != null) {
					setCurrentConditionIcon(result);
				}
			}
		});
	}

	private void setCurrentConditionIcon(final CacheableBitmapDrawable drawable) {
		post(new Runnable() {
			public void run() {
				imageView.setImageDrawable(drawable);
			}
		});
	}
}
