package com.manuelpeinado.droidcast.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.manuelpeinado.droidcast.R;

/**
 * Displays a label and a value to its right
 */
public class KeyValuePairView extends FrameLayout {
	protected static final String TAG = KeyValuePairView.class.getSimpleName();
	private TextView labelView;
	private TextView valueView;
	private String units;
	private String label;

	public KeyValuePairView(Context context) {
		this(context, null);
	}

	public KeyValuePairView(Context context, AttributeSet attrs) {
		super(context, attrs);
		LayoutInflater inflater = LayoutInflater.from(context);
		ViewGroup root = (ViewGroup) inflater.inflate(R.layout.view_key_value_pair, this);
		labelView = (TextView) root.findViewById(R.id.label);
		valueView = (TextView) root.findViewById(R.id.value);
		parseAttrs(context, attrs);
		setValue("-");
	}

	public void setValue(String value) {
		if (label != null) {
			labelView.setText(label + ": ");
		}
		valueView.setText(String.format("%s %s", value, units));
	}

	private void parseAttrs(Context context, AttributeSet attrs) {
		TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.KeyValuePairView, 0, 0);
		label = ta.getString(R.styleable.KeyValuePairView_android_text);
		units = ta.getString(R.styleable.KeyValuePairView_units);
		if (units == null) {
			units = "";
		}
		labelView.setTag(units);
		ta.recycle();
	}

	public String getUnits() {
		return units;
	}
}
