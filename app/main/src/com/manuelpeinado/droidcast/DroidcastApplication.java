package com.manuelpeinado.droidcast;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.app.Application;
import com.manuelpeinado.droidcast.persistence.CitiesDao;
import com.manuelpeinado.droidcast.persistence.Prefs;
import com.manuelpeinado.utils.AppCtx;

/**
 * Allows us to perform some initialization tasks when the application starts
 * Also defines some global constants
 */
public class DroidcastApplication extends Application {
	/** Number of days of the forecast */
	public static final int NUM_DAYS = 5;
	public static final int CITY_LIST_ACTIVITY = 0;
	public static final int FORECAST_ACTIVITY = 1;

	@Override
	public void onCreate() {
		super.onCreate();
		AppCtx.set(this);
		ActiveAndroid.initialize(this);
		performFirstTimeInitialization();
	}

	private void performFirstTimeInitialization() {
		if (!Prefs.isFirstTime()) {
			return;
		}
		Prefs.setFirstTime(false);
		initDb();
	}

	private void initDb() {
		String[] cityNames = getResources().getStringArray(R.array.initial_cities);
		new CitiesDao().addAll(cityNames);
	}
}
