package com.manuelpeinado.droidcast.fragments;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.manuelpeinado.droidcast.DroidcastApplication;
import com.manuelpeinado.droidcast.R;
import com.manuelpeinado.droidcast.persistence.City;
import com.manuelpeinado.droidcast.views.CurrentConditionView;
import com.manuelpeinado.droidcast.views.DailyForecastView;
import com.manuelpeinado.droidcast.weather_api.Forecast;
import com.manuelpeinado.droidcast.weather_api.WorldWeatherOnlineClient;
import com.manuelpeinado.utils.Callback;

/**
 * Shows the weather forecast of a particular city
 */
public class CityForecastFragment extends Fragment {
	protected static final String TAG = CityForecastFragment.class.getSimpleName();
	private City city;
	private Forecast forecast;
	private CurrentConditionView currentConditionView;
	private List<DailyForecastView> weatherViews = new ArrayList<DailyForecastView>();
	private LinearLayout containerView;

	public static CityForecastFragment newInstance(City city) {
		Bundle args = new Bundle();
		args.putParcelable("city", city);
		CityForecastFragment result = new CityForecastFragment();
		result.setArguments(args);
		return result;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_forecast, container, false);
		currentConditionView = (CurrentConditionView)root.findViewById(R.id.currentCondition);
		containerView = (LinearLayout)root.findViewById(R.id.container);
		addForecastViews();
		return root;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		city = getArguments().getParcelable("city");
		if (savedInstanceState != null && savedInstanceState.containsKey("forecast")) {
			forecast = savedInstanceState.getParcelable("forecast");
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		if (forecast == null) {
			WorldWeatherOnlineClient client = new WorldWeatherOnlineClient();
			client.getForecast(city, forecastCallback);
		}
		else {
			setForecast(forecast);
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putParcelable("forecast", forecast);
	}

	public void refreshForecast() {
		WorldWeatherOnlineClient client = new WorldWeatherOnlineClient();
		client.setIgnoreCache(true);
		client.getForecast(city, forecastCallback);
	}
	
	private Callback<Forecast> forecastCallback = new Callback<Forecast>() {
		public void call(Forecast result) {
			setForecast(result);
		}
	};
	
	private void setForecast(Forecast forecast) {
		this.forecast = forecast;
		currentConditionView.setCurrentCondition(forecast.getCurrentCondition());
		for (int i = 0; i < DroidcastApplication.NUM_DAYS; ++i) {
			weatherViews.get(i).setWeather(forecast.getWeather(i));
		}
	}
	
	private void addForecastViews() {
		weatherViews.clear();
		for (int i = 0; i < DroidcastApplication.NUM_DAYS; ++i) {
			addForecastView();
			boolean isLast = i == DroidcastApplication.NUM_DAYS - 1;
			if (!isLast) {
				addHorizontalDivider();
			}
		}
	}

	private void addForecastView() {
		DailyForecastView newView = new DailyForecastView(getActivity());
		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		containerView.addView(newView, params);
		weatherViews.add(newView);
	}

	private void addHorizontalDivider() {
		LayoutInflater inflate = LayoutInflater.from(getActivity());
		View divider = inflate.inflate(R.layout.horizontal_divider_dim, containerView, false);
		containerView.addView(divider);
	}
}
