
package com.manuelpeinado.droidcast.fragments;

import java.util.ArrayList;
import java.util.List;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.ActionMode;
import com.manuelpeinado.droidcast.R;
import com.manuelpeinado.droidcast.action_modes.CityActionModeCallback;
import com.manuelpeinado.droidcast.persistence.CitiesDao;
import com.manuelpeinado.droidcast.persistence.City;
import com.manuelpeinado.droidcast.persistence.Db;
import com.manuelpeinado.droidcast.views.CityView;
import com.manuelpeinado.undobar.IUndoActivity;
import com.manuelpeinado.undobar.UndoBarController.UndoListener;
import com.manuelpeinado.utils.AppCtx;

/**
 * Shows a list of all available cities and allows the user to select one of them
 */
public class CityListFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>, 
                                                          CityView.Listener, UndoListener {

	// TODO retain selected cities when activity is recreated (config. change)
	
	private TableLayout table;
	private ActionMode actionMode;
	private List<City> selectedCities = new ArrayList<City>();
	private List<City> deletedCities;
	private Listener listener;
	private TextView headerMessageView;
	private boolean editMode;
	
	public interface Listener {
		void onCityClicked(City city);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_city_list, container, false);
		table = (TableLayout) root.findViewById(R.id.table);
		headerMessageView = (TextView) root.findViewById(R.id.headerMessage);
		return root;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		getLoaderManager().initLoader(0, null, this);
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		return createLoader();
	}
	
	public static Loader<Cursor> createLoader() {
		String orderBy = Db.CitiesTable.NAME_COLUMN + " ASC";
		return new CursorLoader(AppCtx.get(), Db.CitiesTable.CONTENT_URI, null, null, null, orderBy);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		table.removeAllViews();
		List<City> cities = CitiesDao.loadAll(cursor);
		for (City city : cities) {
			CityView row = new CityView(getActivity());
			row.setCity(city);
			row.setListener(this);
			table.addView(row);
		}
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
	}

	@Override
	public void onCityChecked(CityView sender, boolean isChecked) {
		if (isChecked) {
			selectedCities.add(sender.getCity());
		}
		else {
			selectedCities.remove(sender.getCity());
		}
		int count = selectedCities.size();
		if (count == 0) {
			finishActionMode();
			return;
		}
		if (count == 1 && actionMode == null)  {
			actionMode = getSherlockActivity().startActionMode(new CityActionModeCallback(this));
		}
		String description = getResources().getQuantityString(R.plurals.selected_cities, count, count);
		actionMode.setTitle(description);
	}

	@Override
	public void onCityClicked(CityView sender, City city) {
		if (editMode) {
			return;
		}
		if (listener != null) {
			listener.onCityClicked(city);
		}
	}
	
	private SherlockFragmentActivity getSherlockActivity() {
		return (SherlockFragmentActivity) getActivity();
	}

	public void clearSelection() {
		for (int i = 0; i < table.getChildCount(); ++i) {
			CityView child = (CityView) table.getChildAt(i);
			child.uncheck(false);
		}
		actionMode = null;
		selectedCities.clear();
	}

	public void deleteSelectedCities() {
		new CitiesDao().deleteAll(selectedCities);
		deletedCities = new ArrayList<City>(selectedCities);
		selectedCities.clear();
		finishActionMode();
		showUndoBar();
	}

	private void showUndoBar() {
		if (getActivity() instanceof IUndoActivity) {
			int n = deletedCities.size();
			IUndoActivity undoActivity = (IUndoActivity)getActivity();
			String message = getResources().getQuantityString(R.plurals.deleted_cities, n, n);
			undoActivity.showUndoBar(this, message, "citiesDeleted");
		}
	}

	private void finishActionMode() {
		actionMode.finish();
		actionMode = null;
	}

	@Override
	public void onUndo(String token) {
		if (!"citiesDeleted".equals(token)) {
			return;
		}
		new CitiesDao().saveAll(deletedCities);
		deletedCities.clear();
	}

	public int getSelectedCount() {
		return selectedCities.size();
	}

	public City getSingleSelection() {
		if (getSelectedCount() != 1) {
			throw new IllegalStateException();
		}
		return selectedCities.get(0);
	}
	
	public void setListener(Listener listener) {
		this.listener = listener;
	}

	public void startEditMode() {
		editMode = true;
		headerMessageView.setVisibility(View.INVISIBLE);
	}

	public void endEditMode() {
		editMode = false;
		headerMessageView.setVisibility(View.VISIBLE);
	}
}
