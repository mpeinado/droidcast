package com.manuelpeinado.droidcast.persistence;

import android.content.Context;
import android.content.SharedPreferences;

import com.manuelpeinado.droidcast.DroidcastApplication;
import com.manuelpeinado.utils.AppCtx;

/**
 * Provides simplified, type-safe access to application preferences
 */
public class Prefs {

	public static boolean isFirstTime() {
		SharedPreferences prefs = getPrefs();
		return prefs.getBoolean("firstTime", true);
	}

	public static void setFirstTime(boolean value) {
		SharedPreferences prefs = getPrefs();
		prefs.edit().putBoolean("firstTime", value).commit();
	}

	public static int getLastActivity() {
		SharedPreferences prefs = getPrefs();
		return prefs.getInt("activity", DroidcastApplication.CITY_LIST_ACTIVITY);
	}

	public static void setLastActivity(int activityId) {
		SharedPreferences prefs = getPrefs();
		prefs.edit().putInt("activity", activityId).commit();
	}

	public static void clearLastActivity() {
		SharedPreferences prefs = getPrefs();
		prefs.edit().remove("activity").commit();
	}
	
	public static String getLastSelectedCity() {
		SharedPreferences prefs = getPrefs();
		return prefs.getString("city", null);
	}
	
	public static void setLastSelectedCity(String name) {
		SharedPreferences prefs = getPrefs();
		prefs.edit().putString("city", name).commit();
	}


	private static SharedPreferences getPrefs() {
		return AppCtx.get().getSharedPreferences("prefs", Context.MODE_PRIVATE);
	}
}
