package com.manuelpeinado.droidcast.persistence;

import com.manuelpeinado.caching.CachingHttpClient;
import com.manuelpeinado.caching.CachingImageLoader;


/**
 * Provides application-wide access to a generic caching HTTP client and a caching image loader 
 */
public class CacheManager {
	private static CachingImageLoader imageLoader;
	private static CachingHttpClient httpClient;

	public static CachingImageLoader getImageLoader() {
		if (imageLoader == null) {
			imageLoader = new CachingImageLoader("images");
		}
		return imageLoader;
	}

	public static CachingHttpClient getHttpClient() {
		if (httpClient == null) {
			httpClient = new CachingHttpClient("http");
		}
		return httpClient;
	}
}
