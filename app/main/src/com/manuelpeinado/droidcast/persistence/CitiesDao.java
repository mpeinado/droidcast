package com.manuelpeinado.droidcast.persistence;

import java.util.ArrayList;
import java.util.List;

import android.database.Cursor;

import com.activeandroid.query.From;
import com.activeandroid.query.Select;
import com.manuelpeinado.utils.AppCtx;

/**
 * Provides high-level access to the "Cities" DB table
 */
public class CitiesDao {

	public void deleteAll(List<City> selectedCities) {
		for (City city : selectedCities) {
			city.delete();
		}
		notifyDbChange();
	}
	
	public void saveAll(List<City> deletedCities) {
		for (City city : deletedCities) {
			city.save();
		}
		notifyDbChange();
	}
	
	public static List<City> loadAll(Cursor cursor) {
		List<City> result = new ArrayList<City>();
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			City city = new City(cursor);
			result.add(city);
			cursor.moveToNext();
		}
		return result;
	}

	public City queryCityByName(String cityName) {
		From query = new Select().from(City.class);
		query.where(Db.CitiesTable.NAME_COLUMN + " = ?", cityName);
		return query.executeSingle();
	}

	public void add(String cityName) {
		City city = new City();
		city.setName(cityName);
		city.save();
		notifyDbChange();
	}

	public void addAll(String[] cityNames) {
		for (String name : cityNames) {
			City city = new City();
			city.setName(name);
			city.save();
		}
		notifyDbChange();
	}
	
	private void notifyDbChange() {
		AppCtx.get().getContentResolver().notifyChange(Db.CitiesTable.CONTENT_URI, null);
	}

	public int getCount() {
		// TODO this is an extremely inefficient way to compute the count
		From query = new Select(Db.CitiesTable.ID_COLUMN).from(City.class);
		List<City> all = query.execute();
		return all.size();
	}
}
