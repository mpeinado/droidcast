package com.manuelpeinado.droidcast.persistence;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Maps to rows of the "Cities" DB table. We use ActiveAndroid for the mapping
 */
@Table(name = Db.CitiesTable.TABLE_NAME)
public class City extends Model implements Parcelable {
	@Column(name = Db.CitiesTable.NAME_COLUMN)
	private String name;

	public City() {
		this((String)null);
	}
	
	public City(String name) {
		this.name = name;
	}

	public City(Cursor cursor) {
		loadFromCursor(cursor);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void loadFromCursor(Cursor cursor) {
		loadFromCursor(getClass(), cursor);
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		City other = (City) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	// Parcelable methods implemented using http://devk.it/proj/parcelabler/

	protected City(Parcel in) {
		name = in.readString();
	}

	public int describeContents() {
		return 0;
	}

	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(name);
	}

	public static final Parcelable.Creator<City> CREATOR = new Parcelable.Creator<City>() {
		public City createFromParcel(Parcel in) {
			return new City(in);
		}
		public City[] newArray(int size) {
			return new City[size];
		}
	};

	public boolean hasSameNameAs(City city) {
		return name.equalsIgnoreCase(city.getName());
	}
}
