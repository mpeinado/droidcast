package com.manuelpeinado.droidcast.persistence;

import android.net.Uri;

import com.manuelpeinado.utils.AppCtx;


/**
 * Defines database-related constants
 */
public class Db {
	public interface CitiesTable {
		String TABLE_NAME = "Cities";
		String ID_COLUMN = "id";
		String NAME_COLUMN = "Name";
		String POSITION_COLUMN = "Position";
		Uri CONTENT_URI = Uri.parse("content://" + AppCtx.get().getPackageName() + "/" + TABLE_NAME.toLowerCase());
	}
	public interface HttpCacheTable {
		String TABLE_NAME = "HttpCache";
		String ID_COLUMN = "id";
		String URL_COLUMN = "Url";
		String TIME_STAMP_COLUMN = "Timestamp";
		Uri CONTENT_URI = Uri.parse("content://" + AppCtx.get().getPackageName() + "/" + TABLE_NAME.toLowerCase());
	}
}
