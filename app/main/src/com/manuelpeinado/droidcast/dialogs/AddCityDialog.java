package com.manuelpeinado.droidcast.dialogs;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;

import com.manuelpeinado.droidcast.R;
import com.manuelpeinado.utils.Utils;

/**
 * Lets the user enter the name of a city
 */
// TODO use GooglePlaces API to autocomplete the text entered by the user
// TODO make more general and move to Utils
public class AddCityDialog extends DialogFragment implements OnClickListener {

	private Listener listener;
	private EditText cityNameView;

	public interface Listener {
		void onCityAccepted(String cityName);
	}

	public void setListener(Listener listener) {
		this.listener = listener;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		ViewGroup root = (ViewGroup) inflater.inflate(R.layout.dialog_add_city, container, false);
		root.findViewById(R.id.ok).setOnClickListener(this);
		root.findViewById(R.id.cancel).setOnClickListener(this);
		cityNameView = (EditText) root.findViewById(R.id.cityName);
		setTitle();
		return root;
	}
	
	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.ok:
			onOk();
			return;
		case R.id.cancel:
			onCancel();
			return;
		}
	}

	private void onOk() {
		String currentText = getCurrentText();
		if (currentText.isEmpty()) {
			Utils.shortToast(R.string.you_must_enter_a_valid_value);
		}
		if (listener != null) {
			currentText = Utils.capitalize(currentText);
			listener.onCityAccepted(currentText);
		}
		dismiss();
	}

	private String getCurrentText() {
		return cityNameView.getText().toString().trim();
	}

	private void onCancel() {
		dismiss();
	}
	
	private void setTitle() {
		getDialog().setTitle(R.string.add_city);
	}

}
