package com.manuelpeinado.caching;

import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.manuelpeinado.utils.Callback;

/**
 * An asynchronous HTTP client which caches responses both using a disk-based LRU cache
 */
public class CachingHttpClient {
	private static final String TAG = CachingHttpClient.class.getSimpleName();
	private HttpDiskLruCache diskLruCache;

	public CachingHttpClient(String uniqueId) {
		diskLruCache = new HttpDiskLruCache(uniqueId);
	}
	
	public void get(final String url, final String cacheKey, final Callback<String> responseHandler) {
		diskLruCache.getCachedResponse(cacheKey, new Callback<String>() {
			public void call(String resp) {
				if (resp != null) {
					Log.v(TAG, "Retrieving response to " + url + " from cache");
					responseHandler.call(resp);
					return;
				}
				downloadResponse(url, cacheKey, responseHandler);
			}
		});
	}
	
	public void removeFromCache(String cacheKey) {
		diskLruCache.removeFromCache(cacheKey);
	}
	
	private void downloadResponse(final String url, final String cacheKey, final Callback<String> responseHandler) {
		Log.v(TAG, "Sending HTTP request for" + url);
		AsyncHttpClient client = new AsyncHttpClient();
		client.get(url, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String responseText) {
				Log.v(TAG, "Adding response from " + url + " to cache");
				diskLruCache.putResponse(cacheKey, responseText);
				responseHandler.call(responseText);
			}
		});
	}
}
