package com.manuelpeinado.caching;

import java.io.ByteArrayInputStream;

import uk.co.senab.bitmapcache.CacheableBitmapDrawable;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.BinaryHttpResponseHandler;
import com.manuelpeinado.utils.Callback;

/**
 * Downloads images asynchronously and caches them both in memory and in disk
 */
public class CachingImageLoader {
	protected static final String TAG = CachingImageLoader.class.getSimpleName();
	private BitmapCache bitmapCache;

	public CachingImageLoader(String uniqueId) {
		bitmapCache = new BitmapCache(uniqueId);
	}

	public void getDrawable(final String url, final Callback<CacheableBitmapDrawable> callback) {
		bitmapCache.getDrawable(url, new Callback<CacheableBitmapDrawable>() {
			@Override
			public void call(CacheableBitmapDrawable result) {
				if (result != null) {
					Log.v(TAG, "Retrieving bitmap " + url + " from cache");
					callback.call(result);
					return;
				}
				downloadDrawable(url, callback);
			}
		});
	}

	private void downloadDrawable(final String url, final Callback<CacheableBitmapDrawable> callback) {
		Log.v(TAG, "Downloading bitmap " + url);
		AsyncHttpClient client = new AsyncHttpClient();
		client.get(url, new BinaryHttpResponseHandler() {
			@Override
			public void onSuccess(final byte[] bytes) {
				Log.v(TAG, "Adding bitmap " + url + " to cache");
				ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
				bitmapCache.putBitmap(url, bis, callback);
			}
		});
	}
}
