package com.manuelpeinado.caching;

import java.io.File;
import java.io.IOException;

import android.os.AsyncTask;
import android.util.Log;

import com.jakewharton.DiskLruCache;
import com.jakewharton.DiskLruCache.Editor;
import com.jakewharton.DiskLruCache.Snapshot;
import com.manuelpeinado.utils.Callback;
import com.manuelpeinado.utils.Utils;

/**
 * A disk-based LRU cache for HTTP responses. Entries expire after N minutes
 */
public class HttpDiskLruCache {
	protected static final String TAG = HttpDiskLruCache.class.getSimpleName();
	private DiskLruCache diskLruCache;
	private static final int RESPONSE_TEXT = 0;
	private static final int TIME_STAMP = 1;
	/** Http responses will not be cached for longer than this (milliseconds) */
	private static final int MAX_CACHE_AGE = 60 * 10 * 1000;
	private Object lock = new Object();

	public HttpDiskLruCache(final String uniqueId) {
		new Thread(new Runnable() {
			public void run() {
				synchronized (lock) {
					openCache(uniqueId);
				}
			}
		}).start();
	}

	public void getCachedResponse(final String key, final Callback<String> responseHandler) {
		if (diskLruCache == null) {
			responseHandler.call(null);
			return;
		}
		new AsyncTask<Void, Void, String>() {
			protected String doInBackground(Void... params) {
				synchronized (lock) {
					return getCachedResponseImpl(key);
				}
			}
			@Override
			protected void onPostExecute(String result) {
				responseHandler.call(result);
			}
		}.execute();
	}

	public void putResponse(final String key, final String responseText) {
		new Thread(new Runnable() {
			public void run() {
				synchronized (lock) {
					putResponseImpl(key, responseText);
				}
			}
		}).start();
	}
	
	public void removeFromCache(final String cacheKey) {
		new Thread(new Runnable() {
			public void run() {
				synchronized (lock) {
					removeFromCacheImpl(cacheKey);
				}
			}
		}).start();
	}

	private void openCache(final String uniqueId) {
		File dir = Utils.getDiskCacheDir(uniqueId);
		try {
			diskLruCache = DiskLruCache.open(dir, 1, 2, 10 * 1024 * 1024);
		} catch (IOException e) {
			e.printStackTrace();
			diskLruCache = null;
		}
	}

	private String getCachedResponseImpl(String key) {
		try {
			Snapshot snapshot = diskLruCache.get(key);
			if (snapshot == null) {
				Log.v(TAG, "No response cache found for key " + key);
				return null;
			}
			Log.v(TAG, "Response cache found for key " + key);
			long timeStamp = Long.parseLong(snapshot.getString(TIME_STAMP));
			if (!isItemCacheFresh(timeStamp)) {
				diskLruCache.remove(key);
				return null;
			}
			return snapshot.getString(RESPONSE_TEXT);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	private void putResponseImpl(final String key, final String responseText) {
		try {
			Editor editor = diskLruCache.edit(key);
			editor.set(RESPONSE_TEXT, responseText);
			editor.set(TIME_STAMP, Long.toString(System.currentTimeMillis()));
			editor.commit();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void removeFromCacheImpl(final String cacheKey) {
		try {
			diskLruCache.remove(cacheKey);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private boolean isItemCacheFresh(long cacheTimeStamp) {
		long currentTime = System.currentTimeMillis();
		long cachedItemAge = currentTime - cacheTimeStamp;
		return cachedItemAge < MAX_CACHE_AGE;
	}
}
