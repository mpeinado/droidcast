package com.manuelpeinado.caching;

import java.io.File;
import java.io.InputStream;

import uk.co.senab.bitmapcache.BitmapLruCache;
import uk.co.senab.bitmapcache.CacheableBitmapDrawable;

import com.manuelpeinado.utils.Callback;
import com.manuelpeinado.utils.Utils;

/**
 * A disk-based LRU cache for HTTP responses. Entries expire after N minutes. 
 * This is actually a thin wrapper around Chris Banes' BitmapLruCache
 */
public class BitmapCache {
	protected static final String TAG = BitmapCache.class.getSimpleName();
	private BitmapLruCache bitmapLruCache;

	public BitmapCache(String uniqueId) {
		BitmapLruCache.Builder builder = new BitmapLruCache.Builder();
		builder.setDiskCacheEnabled(true);
		File dir = Utils.getDiskCacheDir(uniqueId);
		builder.setDiskCacheLocation(dir);
		builder.setDiskCacheMaxSize(10 * 1024 * 1024);
		builder.setDiskCacheMaxSize(10 * 1024 * 1024);
		builder.setMemoryCacheEnabled(true);
		builder.setMemoryCacheMaxSizeUsingHeapSize();
		bitmapLruCache = builder.build(); 
	}

	public void getDrawable(final String url, final Callback<CacheableBitmapDrawable> callback) {
		// TODO invoke the callback in the main thread using an asynctask
		new Thread(new Runnable() {
			@Override
			public void run() {
				callback.call(bitmapLruCache.get(url));
			}
		}).start();
	}

	public void putBitmap(final String url, final InputStream is, final Callback<CacheableBitmapDrawable> callback) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				callback.call(bitmapLruCache.put(url, is));
			}
		}).start();
	}
}
