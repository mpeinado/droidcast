package com.manuelpeinado.utils;

/**
 * Generic callback for asynchronous operations
 */
public interface Callback<T> {
	void call(T result);
}
