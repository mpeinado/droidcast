package com.manuelpeinado.utils;

import android.content.Context;

/**
 * Offers access to the global application context from anywhere in our application
 * The method AppCtx.set() should be called from within Application.onCreate()
 */
public class AppCtx {
	private static Context ctx;

	public static Context get() {
		if (ctx == null) {
			throw new IllegalStateException("AppCtx.set() has not been called");
		}
		return ctx;
	}
	
	public static void set(Context ctx) {
		if (ctx == null) {
			throw new NullPointerException("Context cannot be null");
		}
		AppCtx.ctx = ctx;
	}
}
