package com.manuelpeinado.utils;

import java.io.File;

import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.text.TextUtils;
import android.widget.Toast;

/**
 * Provides utility methods that can be reused between applications
 *
 */
public class Utils {

	public static void shortToast(String text) {
		Toast.makeText(AppCtx.get(), text, Toast.LENGTH_SHORT).show();
	}
	
	public static void shortToast(int stringResId) {
		Toast.makeText(AppCtx.get(), stringResId, Toast.LENGTH_SHORT).show();
	}


	// Taken from here: http://stackoverflow.com/a/10235381/244576
	public static File getDiskCacheDir(String uniqueName) {
		// Check if media is mounted or storage is built-in, if so, try and use
		// external cache dir otherwise use internal cache dir
		Context ctx = AppCtx.get();
		final String cachePath = Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()) 
								 || !isExternalStorageRemovable() ? getExternalCacheDir(ctx).getPath()
										 				 		  : ctx.getCacheDir().getPath();
		return new File(cachePath + File.separator + uniqueName);
	}

	public static boolean isExternalStorageRemovable() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
			return Environment.isExternalStorageRemovable();
		}
		return true;
	}

	public static File getExternalCacheDir(Context context) {
		if (hasExternalCacheDir()) {
			return context.getExternalCacheDir();
		}

		// Before Froyo we need to construct the external cache dir ourselves
		final String cacheDir = "/Android/data/" + context.getPackageName() + "/cache/";
		return new File(Environment.getExternalStorageDirectory().getPath() + cacheDir);
	}

	public static boolean hasExternalCacheDir() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO;
	}
	
    // Adapted from Apache commons-lang
    public static String capitalize(String str) {
    	return capitalize(str, null);
    }

    public static String capitalize(String str, char... delimiters) {
       final int delimLen = delimiters == null ? -1 : delimiters.length;
       if (TextUtils.isEmpty(str) || delimLen == 0) {
           return str;
       }
       final char[] buffer = str.toCharArray();
       boolean capitalizeNext = true;
       for (int i = 0; i < buffer.length; i++) {
           final char ch = buffer[i];
           if (isDelimiter(ch, delimiters)) {
               capitalizeNext = true;
           } else if (capitalizeNext) {
               buffer[i] = Character.toTitleCase(ch);
               capitalizeNext = false;
           }
       }
       return new String(buffer);
    }
    
    // Adapted from Apache commons-lang
    private static boolean isDelimiter(char ch, char[] delimiters) {
		if (delimiters == null) {
			return Character.isWhitespace(ch);
		}
		for (final char delimiter : delimiters) {
			if (ch == delimiter) {
				return true;
			}
		}
		return false;
    }
}
