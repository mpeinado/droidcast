package com.manuelpeinado.undobar;

import com.manuelpeinado.undobar.UndoBarController.UndoListener;


public interface IUndoActivity {

	void showUndoBar(UndoListener listener, String message, String undoToken);

}
